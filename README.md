
# DataEggs: open datasets for Eclipse


[Eclipse DataEggs](https://www.eclipse.org/dataeggs) provides datasets related to the development of Eclipse projects.

This repository contains resources used to extract development-related data from the Eclipse forge, and format, clean, and publish it to the [project's website](https://www.eclipse.org/dataeggs). 

The datasets provided include:

* [Mailing lists](https://www.eclipse.org/dataeggs/eclipse_mls) (full mboxes and csv extracts) hosted at the Eclipse forge with their [documentation and examples](https://download.eclipse.org/dataeggs/eclipse_mls/mbox_csv_analysis).
* [AERI exception stacktraces](https://www.eclipse.org/dataeggs/aeri_stacktraces) (not updated anymore, historical data only) includes 2 datasets: problems (see [documentation](https://www.eclipse.org/dataeggs/aeri_stacktraces/problems_analysis.pdf)) and incidents (see [documentation](https://www.eclipse.org/dataeggs/aeri_stacktraces/problems_incidents.pdf)).
* [Development data](https://www.eclipse.org/dataeggs/projects/) from Eclipse projects. Depending on data sources, the following information is provided:
  - SCM (git).
  - ITS (Bugzilla, GitHub issues, GitLab issues).
  - CI (Jenkins).
  - PMI checks.
  - Stack Overflow statistics.
  - Scancode analysis (executed on our server).

Privacy has been a major concern from the beginning. Once extracted, data is anonymised using [data-anonymiser](https://github.com/borisbaldassari/data-anonymiser) and published in the downloads section of the project. See [our documentation for more details](https://download.eclipse.org/scava/privacy/)

## Licencing

The default licence for code in this repository is the [Eclipse Public Licence, v2](https://www.eclipse.org/legal/epl-2.0/).

All datasets are published under the [Creative Commons BY-Attribution-Share Alike 4.0 (International)](https://creativecommons.org/licenses/by-sa/4.0/).

## Repository structure 

* **scripts** folder contains the code for the extraction, anonymisation and publication of the datasets. It is mostly composed of shell scripts.
* Every dataset, once generated, is fed to a R Markdown document for analysis. This serves both as a test (check if values seem consistent) and as a presentation of the data through plots and tables. R Markdown files are located in the **report** folder.
* The final website uses [Hugo](https://gohugo.io/) and [Blogdown](https://github.com/rstudio/blogdown). Once the RMarkdown files have been deployed with the scripts, one can generate the website by starting a R session in the **website** directory and running the following sequence:

```
require(blogdown)
blogdown::build_site(build_rmd=T)
```


## Prerequisites

* Perl: Mail::Box::Manager Text::CSV DateTime::Format::Strptime Encoding::FixLatin
* R: blogdown googleVis tm wordcloud SnowballC xts dygraphs xtable R.utils rmarkdown
