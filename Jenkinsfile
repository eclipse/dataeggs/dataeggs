pipeline {
  agent any
  options {
    buildDiscarder(logRotator(numToKeepStr: '10', artifactNumToKeepStr: '5'))
  }
  stages {
    stage('Setup AERI') {
      steps {
        sh '''
          cp /data/aeri/* website/content/aeri_stacktraces
        ''' 
      }
    }
    stage('Setup mboxes') {
      steps {
        sh '''
          ls /data/eclipse_mls/ > website/content/eclipse_mls/list_mboxes.txt
          cp /data/eclipse_mls_full.csv.gz website/content/eclipse_mls/eclipse_mls.csv.gz
          cp /data/eclipse_mls_full.csv website/content/eclipse_mls/eclipse_mls_full.csv
          cp -r /data/eclipse_mls_scrambled/ website/content/eclipse_mls/mboxes/
        ''' 
      }
    }

    stage('Build projects') {
      steps {
        sh '''
          pwd
          ls
          source ~/perl5/perlbrew/etc/bashrc 
          perlbrew switch perl-5.30.1
          perl -v 
          Rscript --version
          cd scripts/ && sh ./process_all_projects.sh
        ''' 
      }
    }
    stage('Build website') {
      steps {
        sh '''
          pwd
          ls
          source ~/perl5/perlbrew/etc/bashrc 
          Rscript --version
          echo "Building website."
          cd scripts/ && sh ./build_website.sh
          echo "Cleaning website zone from non-compressed files."
          find ../website/public/ -name "*.csv" | xargs rm -rf 
          find ../website/public/ -name "git*.txt" | xargs rm -rf 
        ''' 
      }
    }
    stage('Build download') {
      steps {
        sh '''
          echo "Creating download area."
          rsync -am --include='*.bz2' --include='*/' --exclude='*' website/public/ download/
          rsync -am --include='*.xz' --include='*/' --exclude='*' website/public/ download/
          rsync -am --include='*.gz' --include='*/' --exclude='*' website/public/ download/
          echo "Cleaning website zone from compressed files."
          find website/public/ -name "*.gz" -or -name "*.xz" -or -name "*.bz2" | xargs rm -rf 
        ''' 
      }
    }
    stage('Deploy Downloads') {
      steps {
        sh '''
          rsync -avz download/ bbaldassari2kd@projects-storage.eclipse.org:/home/data/httpd/download.eclipse.org/dataeggs/ --delete || echo "Error $?: some files/attrs were not transferred (see previous errors)"
        ''' 
      }
    }
    stage('Deploy Website') {
      steps {
        sh '''
          DATE=$(date +"%Y-%m-%d %T")
          rm -rf www/
          git clone ssh://bbaldassari2kd@git.eclipse.org:29418/www.eclipse.org/dataeggs.git www/
		  cd www/
          rm -rf *
          mv ../website/public/* .
          git add -A . 
          git commit -m "Publish web site $DATE."
          git push -u origin master
          
        ''' 
      }
    }
    stage('Archiving') {
      steps {
        archiveArtifacts artifacts: 'www/**/*.*', fingerprint: true 
        archiveArtifacts artifacts: 'download/**/*.*', fingerprint: true 
        cleanWs()
      }
    }
  }
}

