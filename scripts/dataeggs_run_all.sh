##################################################################
#
# Copyright (C) 2022 Boris Baldassari
#
# This program and the accompanying materials are made
# available under the terms of the Eclipse Public License 2.0
# which is available at https://www.eclipse.org/legal/epl-2.0/
#
# SPDX-License-Identifier: EPL-2.0
#
###################################################################

#
# Execute all steps to generate the dataeggs website.
# 

cd scripts/

echo '# Process all projects'
sh ./process_all_projects.sh

echo "# Building website."
sh ./build_website.sh

cd ..

echo "# Cleaning website zone from non-compressed files."
find website/public/ -name "*.csv" | xargs rm -rf 
find website/public/ -name "git*.txt" | xargs rm -rf 

echo "# Creating download area."
rsync -am --include='*.bz2' --include='*/' --exclude='*' website/public/ download/
rsync -am --include='*.xz' --include='*/' --exclude='*' website/public/ download/
rsync -am --include='*.gz' --include='*/' --exclude='*' website/public/ download/
echo "Cleaning website zone from compressed files."
find website/public/ -name "*.gz" -or -name "*.xz" -or -name "*.bz2" | xargs rm -rf 

echo "# Deploying downloads."
rsync -avz download/ bbaldassari2kd@projects-storage.eclipse.org:/home/data/httpd/download.eclipse.org/dataeggs/ --delete || echo "Error $?: some files/attrs were not transferred (see previous errors)"

echo "# Deploy website"
DATE=$(date +"%Y-%m-%d %T")
rm -rf www/
git clone git@gitlab.eclipse.org:eclipse/dataeggs/dataeggs-website.git www/
cd www/
rm -rf *
mv ../website/public/* .

git add -A
if ! git diff --cached --exit-code; then
    echo "Changes have been detected, publishing to repo 'www.eclipse.org/${PROJECT_NAME}'"
    git config user.email "boris.baldassari@gmail.com"
    git config user.name "Boris Baldassari"
    git commit -m "Website build $DATE."
    git log --graph --abbrev-commit --date=relative -n 5
    git push origin HEAD:master
else
    echo "No changes have been detected since last build, nothing to publish"
fi
