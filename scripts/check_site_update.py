#! python3

######################################################################
# Copyright (c) 2017 Castalia Solutions
#
# This program and the accompanying materials are made
# available under the terms of the Eclipse Public License 2.0
# which is available at https://www.eclipse.org/legal/epl-2.0/
#
# SPDX-License-Identifier: EPL-2.0
######################################################################

from datetime import date
import requests
import re

def get_last_sunday():
    from dateutil.relativedelta import relativedelta, SU
    today = date.today()
    last_monday = today + relativedelta(weekday=SU(-1))
    return last_monday.strftime("%Y-%m-%d")

# Build date to check.
last_sunday = get_last_sunday()
print(" - Last Sunday was", last_sunday)

# Fetch page of projects and check page is up-to-date.
with open("list_projects.txt") as f:
    projects = [line.rstrip('\n') for line in f]
    errors = 0
for p in projects:
    if not p: 
        continue
    url = 'https://www.eclipse.org/dataeggs/projects/' + p + '/datasets_report/'
    print("* Checking project", p, "...")
    print("  URL:", url, "")
    r = requests.get(url)
    #print("R TEXT", r.text)
    re_tag = re.compile(".*(" + last_sunday + ").*")
    mydate = re_tag.search(r.text).group(1)

    if mydate:
        print("  Project", p, "is up-to-date.")
    else:
        print("ERROR: cannot find", last_synday, "in report.")
        errors += 1

if errors:
    print("####################################################")
    print("#Found", errors, "errors. Please check the logs.")
    print("####################################################")
    exit(-1)


