##################################################################
#
# Copyright (C) 2019 Castalia Solutions
#
# This program and the accompanying materials are made
# available under the terms of the Eclipse Public License 2.0
# which is available at https://www.eclipse.org/legal/epl-2.0/
#
# SPDX-License-Identifier: EPL-2.0
#
###################################################################


DATE=`date +%Y-%m-%d`
dir_session=/tmp/r_extract_project_${DATE}.session
dir_anon="data-anonymiser"
dir_mbox=/data/eclipse_mls
dir_mbox_scr=/data/eclipse_mls_scrambled
dir_scripts=`pwd`
url_anon="https://github.com/borisbaldassari/data-anonymiser/archive/master.zip"

run_csv=1
run_mbox=1

verbose='true'


echo "# Checking if anonymisation utility is present.."
if [ -e ${dir_anon}/code/anonymise ]; then
    echo "  * Found [${dir_anon}/code/anonymise] utility."
else
    echo "  * Cannot find [${dir_anon}/code/anonymise] utility."
    echo "    Downloading utility from [$url_anon].."
    curl -L -s ${url_anon} -o data-anonymiser.zip
    echo "    Uncompressing utility to ${dir_anon}."
    unzip -q data-anonymiser.zip
    mv data-anonymiser-master/ data-anonymiser/
fi

echo "# Creating session directory [${dir_session}]."
if [ -e $dir_session ]; then
    echo "  * Removing old session dir."
    mv $dir_session ${dir_session}.old
fi


if [ $run_csv -eq 1 ]; then
    echo "# Regenerating CSV files.."

    if [ -d csv/ ]; then
	rm -rf csv/
    fi
    mkdir -p csv/

    for f in `ls ${dir_mbox}`; do
	echo "* Working on mbox $f"
	perl ${dir_scripts}/mbox2csv.pl ${dir_mbox}/$f
	mv $f.csv csv/
    done

    # Generate a single big file with all csv's
    echo "# Regenerating big CSV file.."
    cat headers.init > eclipse_mls_full.csv
    cat csv/*.csv >> eclipse_mls_full.csv
	
    echo "# Checking file eclipse_mls_full.csv is in current dir.."
	if [ -e eclipse_mls_full.csv ]; then
	    echo "  'eclipse_mls_full.csv' found in dir ["`pwd`"]"
	else 
	    echo "  'eclipse_mls_full.csv' NOT found in dir ["`pwd`"]"
		echo "  Listing of files in current directory: "
		ls
		exit 4
	fi
	
    echo "# Anonymising csv dataset"
    perl -Idata-anonymiser/code/ anonymise_csv.pl eclipse_mls_full.csv
    rm eclipse_mls_full.csv
    
    echo "# Checking file eclipse_mls_full_out.csv is in current dir.."
	if [ -e eclipse_mls_full_out.csv ]; then
	    echo "  'eclipse_mls_full_out.csv' found in dir ["`pwd`"]"
	else 
	    echo "  'eclipse_mls_full_out.csv' NOT found in dir ["`pwd`"]"
		echo "  Listing of files in current directory: "
		ls
		exit 4
	fi

    echo "# Cleaning workspace"
    rm -rf csv/

    echo "# Copy eclipse_mls_full.csv to datasets directory..."
    cp eclipse_mls_full_out.csv ${dir_scripts}/../website/content/eclipse_mls/eclipse_mls_full.csv
	
    echo "# Checking file eclipse_mls_full.csv is in website/content/eclipse_mls..."
    if [ -e ${dir_scripts}/../website/content/eclipse_mls/eclipse_mls_full.csv ]; then
	echo "  'eclipse_mls_full.csv' found in dir [${dir_scripts}/website/content/eclipse_mls/]"
    else 
	echo "  'eclipse_mls_full.csv' NOT found in dir [${dir_scripts}/website/content/eclipse_mls/]"
	echo "  Listing of files in directory: "
	ls ${dir_scripts}/../website/content/eclipse_mls/
	exit 4
    fi
    
    echo "# Creating gzip archive eclipse_mls_full.csv.gz"
    gzip -f -k ${dir_scripts}/../website/content/eclipse_mls/eclipse_mls_full.csv
	
    echo "# Checking file eclipse_mls_full.csv.gz is in datasets/eclipse_mls..."
    if [ -e ${dir_scripts}/../website/content/eclipse_mls/eclipse_mls_full.csv.gz ]; then
	echo "  'eclipse_mls_full.csv.gz' found in dir [${dir_scripts}/website/content/eclipse_mls/]"
    else 
	echo "  'eclipse_mls_full.csv.gz' NOT found in dir [${dir_scripts}/website/content/eclipse_mls/]"
	echo "  Listing of files in directory: "
	ls ${dir_scripts}/../website/content/eclipse_mls/
	exit 4
    fi
    
    cp ${dir_scripts}/../website/content/eclipse_mls/eclipse_mls_full.csv /data/
    cp ${dir_scripts}/../website/content/eclipse_mls/eclipse_mls_full.csv.gz /data/
fi

if [ $run_mbox -eq 1 ]; then
    echo "# Anonymise mboxes.."

    if [ -d ${dir_mbox_scr}/ ]; then
	    rm -rf ${dir_mbox_scr}/
    fi
    mkdir -p ${dir_mbox_scr}/

    for f in `ls ${dir_mbox}`; do
	echo "* Working on mbox $f"
	perl -Idata-anonymiser/code/ data-anonymiser/code/anonymise scramble -s $dir_session \
             -f ${dir_mbox}/$f -t ${dir_mbox_scr}/${f}
	gzip -f ${dir_mbox_scr}/${f}
    done

    if [ -d ${dir_scripts}/../website/content/eclipse_mls/mboxes/ ]; then
	rm -rf ${dir_scripts}/../website/content/eclipse_mls/mboxes/
    fi
    mkdir -p ${dir_scripts}/../website/content/eclipse_mls/mboxes/
    
    echo "# Moving mls results to datasets directory..."
    cp ${dir_mbox_scr}/* ${dir_scripts}/../website/content/eclipse_mls/mboxes/
fi

echo "# Removing session directory [${dir_session}]."
rm -rf $dir_session
