for i in `ls -d */`; do
    cd $i/
    echo "## $i"
    for f in `ls *.gz`; do echo "  - [$f]($i/$f)"; done
    cd ..
done
