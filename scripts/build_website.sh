##################################################################
#
# Copyright (C) 2019 Castalia Solutions
#
# This program and the accompanying materials are made
# available under the terms of the Eclipse Public License 2.0
# which is available at https://www.eclipse.org/legal/epl-2.0/
#
# SPDX-License-Identifier: EPL-2.0
#
###################################################################


cd ../website/

echo "# Building web site..."

verbose=1

tmpfile=$(mktemp /tmp/r_extract_project.XXXXXX.r)
echo "  * Rendering website in [$(pwd)]." 
cat <<EOF > $tmpfile
require('blogdown')
blogdown::build_site(build_rmd=T);
EOF

if [ "$verbose" -eq 1 ]; then
    Rscript $tmpfile
else
    Rscript $tmpfile >/dev/null 2>&1
fi

